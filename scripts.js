let btnWrap = document.querySelector(".btn-wrapper");
let btn = document.querySelectorAll(".btn");

document.addEventListener("keydown", (event) => {
  btn.forEach(function (e) {
    e.style.backgroundColor = "black";
    e.style.color = "white";
  });
  btn.forEach((e) => {
    e.addEventListener("blur", () => {});
    if (event.code === e.textContent || event.code === "Key" + e.textContent) {
      e.style.backgroundColor = "blue";
    }
  });
});
